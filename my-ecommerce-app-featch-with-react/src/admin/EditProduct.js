import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { useParams, Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Row, Col, Card } from "react-bootstrap";


export default function EditProduct(props){

const { productId } = useParams();
  const {user} = useContext(UserContext);
  const history = useHistory();




const [name, setName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState('');
const [imageUrl, setImageUrl] = useState('');
const [quantity, setQuantity] = useState('');




function editProduct(e){
  e.preventDefault();



  fetch(`http://localhost:4000/api/product/products/${productId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`
    },
  
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);


      if(data === true){
        Swal.fire({
          title: "Successfully updated Product",
          icon: "success",
          text: "You have successfully updated the product"
        })
        
      }else{
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again"
        })
      }
    })
  history.push("/AdminPageAfterLogin");
}










  return (
    
    <Row className = "mt-3 mb-3">
      
      
      <Col xs={12} md={8} className="mx-auto">
    <Card className="cardHighlight p-3">
  <Card.Body>
    
  <Container>
      <h1 className="text-center">Edit Product</h1>
      <Form className="mt-3" onSubmit={(e) => editProduct(e)}>


<Form.Group className="mb-3" controlId="productName">
          <Form.Label>Enter product name</Form.Label>
<Form.Control 
            type="text" 
            placeholder="Product Name" 
            value = {productId.name}
            onChange = { e => setName(e.target.value)}
            required 
          />
</Form.Group>
<Form.Group className="mb-3" controlId="productDescription">
          <Form.Label>Description</Form.Label>
<Form.Control 
            type="text" 
            placeholder="Enter description" 
            value = {description}
            onChange = { e => setDescription(e.target.value)}
            required 
          />
</Form.Group>

<Form.Group className="mb-3" controlId="productPrice">
          <Form.Label>Price</Form.Label>
<Form.Control 
            type="Number" 
            placeholder="Enter price" 
            value = {price}
            onChange = { e => setPrice(e.target.value)}
            required 
          />
</Form.Group>


<Form.Group className="mb-3" controlId="productImageUrl">
          <Form.Label>ImageUrl</Form.Label>
<Form.Control 
            type="text" 
            placeholder="Enter imageUrl" 
            value = {imageUrl}
            onChange = { e => setImageUrl(e.target.value)}
            required 
          />
</Form.Group>


        <Form.Group className="mb-3" controlId="productQuantity">
          <Form.Label>Quantity</Form.Label>

          <Form.Control 
            type="Number" 
            placeholder="Enter quantity" 
            value = {quantity}
            onChange = { e => setQuantity(e.target.value)}
            required 
          />
          

  </Form.Group>       
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>

          
      </Form>
    </Container>
  </Card.Body>  
</Card>   
      </Col>
    </Row>
  )
}







