import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Fragment, useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import { Link, NavLink } from 'react-router-dom';

export default function AppNavbar(){
	return (

<Navbar expand="lg" className="nav navbar navbar-dark mx-auto">
  <Container className="mx-auto">
    <Navbar.Brand as={Link} to="/" className="text-white" exact>MATHWEB</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mx-auto">
        <Nav.Link as={NavLink} to="/products" className="text-white mx-5" exact>ALL PRODUCTS</Nav.Link>
        
        
        <Nav.Link as={NavLink} to="/register" className="text-white mx-5" exact>REGISTER</Nav.Link>
        <Nav.Link as={NavLink} to="/login" className="text-white mx-5" exact>LOGIN</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>


		)
}