import React from "react"
import { Row, Col, Card } from "react-bootstrap";


export default function NewArrival(){
	return (
<Row className="mt-3 mb-3 container-fit justify-content-center">
    <Card.Title>
      <h2 className="text-center my-4 text-white">New Arrivals</h2>
    </Card.Title>
      <Col xs={12} md={3} className="pb-5">
          <Card className="cardHighlight p-3">
          <img src={"https://spng.pngfind.com/pngs/s/339-3394505_women-t-shirt-free-png-image-disney-world.png"} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">Not just fashion but style</h3>
    </Card.Title>
    <Card.Text className="text-center"> 
      Some quick example text to build on the card title and make up the bulk of
      the card content.
    </Card.Text>
  </Card.Body>
</Card>
      </Col>
      <Col xs={12} md={3} className="pb-5">
        <Card className="cardHighlight p-3">
        <img src={"https://i3.cpcache.com/merchandise/161_300x300_Front_Color-Black.jpg?Size=Large&AttributeValue=NA&c=True&region={%22name%22:%22FrontCenter%22,%22width%22:10,%22height%22:9.202864,%22alignment%22:%22TopCenter%22,%22orientation%22:0,%22dpi%22:100,%22crop_x%22:0,%22crop_y%22:0,%22crop_h%22:900,%22crop_w%22:1000,%22scale%22:0,%22template%22:{%22id%22:29473891,%22params%22:{}}}&cid=PUartJBjiF/yg4FdKqiggQ==%20||%20AHiQPtL7P6rQ8abcsKWvcg==%20&Filters=[{%22name%22:%22background%22,%22value%22:%22ddddde%22,%22sequence%22:2}]"} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">fashionable clothing</h3>
    </Card.Title>
    <Card.Text className="text-center"> 
      Some quick example text to build on the card title and make up the bulk of
      the card content.
    </Card.Text>
  </Card.Body>
  </Card>   


      </Col>


<Col xs={12} md={3} className="pb-5">
        <Card className="cardHighlight p-3">
        <img src={"https://i3.cpcache.com/merchandise/161_300x300_Front_Color-Black.jpg?Size=Large&AttributeValue=NA&c=True&region={%22name%22:%22FrontCenter%22,%22width%22:10,%22height%22:9.202864,%22alignment%22:%22TopCenter%22,%22orientation%22:0,%22dpi%22:100,%22crop_x%22:0,%22crop_y%22:0,%22crop_h%22:900,%22crop_w%22:1000,%22scale%22:0,%22template%22:{%22id%22:29473891,%22params%22:{}}}&cid=PUartJBjiF/yg4FdKqiggQ==%20||%20AHiQPtL7P6rQ8abcsKWvcg==%20&Filters=[{%22name%22:%22background%22,%22value%22:%22ddddde%22,%22sequence%22:2}]"} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">Quality Shirt</h3>
    </Card.Title>
    <Card.Text className="text-center"> 
      Some quick example text to build on the card title and make up the bulk of
      the card content.
    </Card.Text>
  </Card.Body>
  </Card>   


      </Col>



<Col xs={12} md={3} className="pb-5">
        <Card className="cardHighlight p-3">
        <img src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0YVIk6uY842OmhiNI6GKuphDuW1PNYy7APFQg-jz9kYKXYngEBiCN93m6xcwTQUyni1M&usqp=CAU"} alt="Logo" />
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">T-Shirt for you</h3>
    </Card.Title>
    <Card.Text className="text-center"> 
      Some quick example text to build on the card title and make up the bulk of
      the card content.
    </Card.Text>
  </Card.Body>
  </Card>   

  
      </Col>


    </Row>

		)
}