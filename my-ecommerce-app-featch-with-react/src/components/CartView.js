import { Fragment, useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { Row, Col, Card } from "react-bootstrap";
import ProductsLoginNav from "../components/ProductsLoginNav"
import Container from 'react-bootstrap/Container';
import Footer from "../components/Footer"
import CartCard from "./CartCard"
import { useContext } from "react";
import { Button } from "react-bootstrap";
import { useHistory, Link } from "react-router-dom";
import userContext from "../UserContext";
import Swal from "sweetalert2";



export default function CartView(){

const [cart, setCart] = useState([])



	



const { user } = useContext(userContext);
const history = useHistory();
	//The useParams hook allows us to retrieve the courseId passed via URL

	
const [products, setProducts] = useState();
	/*const [name, setName] = useState();
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [imageUrl, setImageUrl] = useState("")*/




useEffect(() => {
	

	fetch("http://localhost:4000/api/order/users/myCart", {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token"
				)}`
		},
		
	})
	.then(res => res.json())
	.then(data => {
		
console.log(data);



setCart(data.map(product => {
	return (
		<CartCard key={product._id}  productProp={product} />

		
	)
})
)

	})

}, []);

	return(
		


		<Row className="mt-3 mb-3 container-fit justify-content-center">
		

{cart}         
   


</Row>
		)
}
