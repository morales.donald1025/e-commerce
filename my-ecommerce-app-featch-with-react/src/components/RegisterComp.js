import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Row, Col, Card } from "react-bootstrap";


export default function Register(){


  const {user} = useContext(UserContext);
  const history = useHistory();




const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
const [email, setEmail] = useState('');
const [mobileNumber, setMobileNumber] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
const [age, setAge] = useState('');
const [gender, setGender] = useState('');
const [isActive, setIsActive] = useState(false);



function registerUser(e){
  e.preventDefault();

    fetch('http://localhost:4000/api/users/checkEmail', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email
        })
    })
    .then(res => res.json())
    .then(data => {

        console.log(data);

        if(data === true){

          Swal.fire({
            title: 'Duplicate email found',
            icon: 'error',
            text: 'Kindly provide another email to complete the registration.'  
          });

        }
        else{
          fetch('http://localhost:4000/api/users/register', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                age: age,
                gender: gender,
                email: email,
                mobileNumber: mobileNumber,
                password: password1
            })

          })
          .then(res => res.json())
          .then(data => {
            console.log(data);

            if (data === true) {

                // Clear input fields
                setFirstName('');
                setLastName('');
                setAge('')
                setGender('');
                setEmail('');
                setMobileNumber('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                    title: 'Registration successful',
                    icon: 'success',
                    text: 'Welcome to Mathweb!'
                });

                history.push("/login");

            } else {

                Swal.fire({
                    title: 'Something wrong',
                    icon: 'error',
                    text: 'Please try again.'   
                });

            }
          })
        }


    })



  }








useEffect(() => {
    // Validation to enable the submit buttion when all fields are populated and both passwords match
    if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNumber.length >= Number(11) && age !== '' && gender !== '' ) && (password1 === password2)){

      setIsActive(true);
      
    }
    else{
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNumber, password1, password2])


  return (
    
    <Row className = "mt-3 mb-3">
      
      
      <Col xs={12} md={8} className="mx-auto">
    <Card className="cardHighlight p-3">
  <Card.Body>
    
  <Container>
      <h1>Register</h1>
      <Form className="mt-3" onSubmit={(e) => registerUser(e)}>


<Form.Group className="mb-3" controlId="userFirstName">
          <Form.Label>Enter first name</Form.Label>
<Form.Control 
            type="text" 
            placeholder="First Name" 
            value = {firstName}
            onChange = { e => setFirstName(e.target.value)}
            required 
          />
</Form.Group>
<Form.Group className="mb-3" controlId="userLastName">
          <Form.Label>Last Name</Form.Label>
<Form.Control 
            type="text" 
            placeholder="Enter last name" 
            value = {lastName}
            onChange = { e => setLastName(e.target.value)}
            required 
          />
</Form.Group>

<Form.Group className="mb-3" controlId="userAge">
          <Form.Label>Age</Form.Label>
<Form.Control 
            type="text" 
            placeholder="Enter age" 
            value = {age}
            onChange = { e => setAge(e.target.value)}
            required 
          />
</Form.Group>


<Form.Group className="mb-3" controlId="userGender">
          <Form.Label>Gender</Form.Label>
<Form.Control 
            type="text" 
            placeholder="Enter gender" 
            value = {gender}
            onChange = { e => setGender(e.target.value)}
            required 
          />
</Form.Group>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>

          <Form.Control 
            type="email" 
            placeholder="Enter email" 
            value = {email}
            onChange = { e => setEmail(e.target.value)}
            required 
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.</Form.Text>            </Form.Group>

        <Form.Group className="mb-3" controlId="userMobileNumber">
          <Form.Label>Mobile Number</Form.Label>
<Form.Control 
            type="number" 
            placeholder="Enter Mobile Number" 
            value = {mobileNumber}
            onChange = { e => setMobileNumber(e.target.value)}
            required 
          />
</Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password1}
            onChange = { e => setPassword1(e.target.value)}
            required 
          />
        </Form.Group>
        
        <Form.Group className="mb-3" controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange = { e => setPassword2(e.target.value)}
            required 
          />
        </Form.Group>
       
            { isActive ? 
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          :
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
        }
          
            
        
      </Form>
    </Container>


  </Card.Body>  
</Card>   
      </Col>
    </Row>
  )
}







